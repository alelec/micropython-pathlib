import sys
# Remove current dir from sys.path, otherwise setuptools will peek up our
# module instead of system's.
sys.path.pop(0)
from setuptools import setup
sys.path.append("..")
import sdist_upip

setup(name='micropython-pathlib-full',
      version='1.1',
      description='Micropython port of pathlib',
      long_description='Trimmed down version of cpython builtin pathlib',
      url='https://gitlab.com/alelec/micropython-pathlib',
      author='Andrew Leech',
      author_email='andrew@alelec.net',
      maintainer='Andrew Leech',
      maintainer_email='andrew@alelec.net',
      license='MIT',
      cmdclass={'sdist': sdist_upip.sdist},
      py_modules=['pathlib'])
