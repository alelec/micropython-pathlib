# pathlib for micropython

This package provides a slightly trimmed down version of the pathbli builtin library from cpython.

The original copy came from cpython v3.8.0a1 : https://raw.githubusercontent.com/python/cpython/0185f34ddcf07b78feb6ac666fbfd4615d26b028/Lib/pathlib.py

Functionality for network / socket / symlink / windows has been removed and minor tweaks made for micropython compatibility.  

Added non-standard features:

## Path.rmdir(contents=False)
  By default the directory must be empty (same as cpython)
  New in micropython: the `contents` argument allows for recursive deleting

## Path.scandir()
  New in micropython: Iterate over the files in this directory.  Does not yield any
  result for the special paths '.' and '..'.

## Path.walk()
  New in micropython: Iterate recursively over the files in this directory tree. Yields Path entries for each file/folder

# Disclaimer
This is a relatively large library still so may not be approriate for all micropython boards but does provide a wide range of file handling cpaabilities.
